#!/usr/bin/env perl
#
# Copyright (c) 2009 Abel Abraham Camarillo Ojeda <acamari@the00z.org>
#
# Permission to use, copy, modify, and distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
#
# srtscroll.pl: "scrolls" the time and ids of an srt file by some initial values
#               it means: it adds to the time and id (of each entry) some values

# example: 
#
#   cat file.srt | ./srtscroll '20' '01:02:03,800'
#   
# scrolls  file.srt changing each id (adding to it '20') and adding to each
# timestamp one hour, two minutes, three seconds and 800 milliseconds
#
# you can get timestamps in milliseconds in mplayer using:
#
# while sleep '.1'; do echo "get_time_pos"; done | mplayer -quiet -slave movie.avi
#
# or try using the x and z keys to delay the subs where you like them

# sets debug mode
#my $debug = "yes";

# a doble line terminator delimits a paragraph (srt entry);
# "\r\n" for DOS files, "\n" for everyone else files. (*.srt are commonly DOS
# files)
my $line_terminator = "\r\n";

# id scroll: we are going to add this value to the ids of all entries (can be
# negative)
my $id_scroll;
$id_scroll = shift @ARGV;

# time scroll: we are going to add this value to the time of all entries (can be
# negative)
#
# this value is specified in the form: hours:minutes:seconds,milliseconds 
# the value can have leading 0's (zeroes), if any they will be stripped on
# before conversion.
# 
# limits: hours         = 99
#         minutes       = 59
#         seconds       = 59
#         milliseconds  = 999
my $time_scroll; 
$time_scroll = shift @ARGV;

# read by "paragraphs"
$/ = ($line_terminator x 2);

while (<>) {
# gets the old srt entry
  my ($id, $time, @text);

  ($id, $time, @text) = split $line_terminator;

  my ($time_start, $time_end);

  ($time_start, $time_end) = split /\s*-+>\s*/, $time;

  if (debug("yes") eq "yes") {
  print '-', $id, $line_terminator, 
        '-', $time, $line_terminator,
        '-', join ($line_terminator.'-', @text), ($line_terminator);
  }
# gets the new srt entry
  my ($new_id, $new_time); 

  $new_id = $id + $id_scroll;

  my ($new_time_start, $new_time_end);

# if the scroll is negative
  if ($time_scroll =~ /^-/) {
# removes all minus signs
          my $t = join '', split '-', $time_scroll;
	  $new_time_start = time_min($time_start, $t);
	  $new_time_end   = time_min($time_end,   $t); 
  } else {
          my $t = join '', split '-', $time_scroll;
	  $new_time_start = time_add($time_start, $t);
	  $new_time_end   = time_add($time_end,   $t); 
  }

  $new_time = $new_time_start . " --> " . $new_time_end;

  print debug('+'), $new_id, $line_terminator, 
        debug('+'), $new_time, $line_terminator,
        debug('+'), join ($line_terminator.debug('+'), @text), 
                    ($line_terminator x 2);

 #print "a + b: ", 
 #      join(' ', time_fold(add_zeroes(time_add([qw(0 59 15 300)], [qw(1 1 1
 #                  1)])))) , 
 #      "\n";
}

sub
debug
{
  if (wantarray) {
    return ($debug =~ /yes/i ? @_ : ());
  } else {
    return ($debug =~ /yes/i ? join('',@_) : undef);
  }
}

# high level function...
sub
time_add
{
  return time_fold(add_zeroes(_time_add(
                                [rm_zeroes(time_unfold($_[0]))], 
                                [rm_zeroes(time_unfold($_[1]))])
                             ));
}

sub
time_min
{
  return time_fold(add_zeroes(_time_min(
                                [rm_zeroes(time_unfold($_[0]))], 
                                [rm_zeroes(time_unfold($_[1]))])
                             ));
}

# removes the leading zeroes of a time array
# example: rm_zeroes(time_unfold('00:00:00,000')) = (0,0,0,0)
sub
rm_zeroes
{
  return map {s/^0+//g; $_} @_;
}

# example: add_zeroes('0','2','3','99') = ('00','02','03','099')
sub
add_zeroes
{ 
  my @tmp = @_;

# adds leading zeroes to hours, minutes and seconds
# two zeroes
  for (@tmp[0,1,2]) {
    s/^/00/;
    s/^.*(..)$/$1/;
  }

# adds three leading zeroes
  for (@tmp[3]) {
    s/^/000/;
    s/^.*(...)$/$1/;
  }

  return @tmp;
}

# unfolds a time value, split it by its fields, deleting leading 0's in each
# field
# example: time_unfold("00:00:00,300") = ('00', '00', '00', '300')
sub
time_unfold
{
  my ($_) = @_;
  return (/^([[:digit:]]+):([[:digit:]]+):([[:digit:]]+),([[:digit:]]+)$/);
}

# joins a time value (splitted in a list)
# for adding leading zeroes see add_zeroes()
# example: time_fold("0:0:0,300") = "0:0:0,300"
sub
time_fold
{
  return (join ':', @_[0,1,2]) . "," . $_[3];
}

# low level function
# adds two times (expressed in two arrays ...)
# time_add([$hour1, $minute1, $second1, $millisecond1],
#          [$hour2, $minute2, $second2, $millisecond2]) = 
#         ($hour1 + $hour2, $minute1 + $minute2, $second1 + $second2,
#          $millisecond1 + $millisecond2)
sub
_time_add
{
  my ($time_one, $time_two) = @_;

  my @time_one;
  my @time_two;
  my @time_new; # the result

  @time_one = @$time_one;
  @time_two = @$time_two;

# proceed in a little endian order...
# milliseconds
  $time_new[3] = ($time_one[3] + $time_two[3]) % 1000; 
  $time_one[2]+= 1 if ($time_one[3] + $time_two[3]) > 1000;

# seconds
  $time_new[2] = ($time_one[2] + $time_two[2]) % 60;
  $time_one[1]+= 1 if ($time_one[2] + $time_two[2]) > 60;

# minutes
  $time_new[1] = ($time_one[1] + $time_two[1]) % 60;
  $time_one[0]+= 1 if ($time_one[1] + $time_two[1]) > 60;
# hours
  $time_new[0] = $time_one[0] + $time_two[0];
# we don't round hours...

# returns hours, minute, seconds, milliseconds
  return (@time_new[0,1,2,3]);
}

sub
_time_min
{
  my ($time_one, $time_two) = @_;

  my @time_one;
  my @time_two;
  my @time_new; # the result

  @time_one = @$time_one;
  @time_two = @$time_two;

# proceed in a little endian order...
# milliseconds
  $time_one[2]-= 1 if ($time_one[3] - $time_two[3]) < 0;
  $time_new[3] =  ($time_one[3] - $time_two[3] + 1000) % 1000;
		  
# seconds
  $time_one[1]-= 1 if ($time_one[2] - $time_two[2]) < 0;
  $time_new[2] =  ($time_one[2] - $time_two[2] + 60) % 60;

# minutes
  $time_one[0]-= 1 if ($time_one[1] - $time_two[1]) < 0;
  $time_new[1] =  ($time_one[1] - $time_two[1] + 60) % 60;

# hours
  $time_new[0] =  $time_one[0] - $time_two[0]; 
# we don't round hours...

# returns hours, minute, seconds, milliseconds
  return (@time_new[0,1,2,3]);
}
